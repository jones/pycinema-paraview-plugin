// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-License-Identifier: BSD-3-Clause
#include "pcImaging.h"

#include <vtkDataObject.h>
#include <vtkObjectFactory.h>
#include <vtkInformation.h>

#include <vtkm/filter/clean_grid/CleanGrid.h>
#include <vtkm/rendering/ScalarRenderer.h>

vtkStandardNewMacro(pcImaging);

//----------------------------------------------------------------------------
pcImaging::pcImaging(){
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
};

int pcImaging::FillInputPortInformation(int port, vtkInformation *info) {
  if(port == 0) {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
    return 1;
  }
  return 0;
}

int pcImaging::FillOutputPortInformation(int port, vtkInformation *info) {
  if(port == 0) {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData" );
    return 1;
  }
  return 0;
}

//----------------------------------------------------------------------------
pcImaging::~pcImaging() = default;

int pcImaging::RequestData(vtkInformation *request,
                 vtkInformationVector **inputVector,
                 vtkInformationVector *outputVector){
  std::cout<<"CCCCC"<<std::endl;
  vtkm::filter::clean_grid::CleanGrid filter;
  vtkm::rendering::ScalarRenderer renderer;
  // // renderer.SetInput(dataSet);
  // renderer.SetDefaultValue(0.0);
  // renderer.SetWidth(512);
  // renderer.SetHeight(512);
  std::cout<<"CCCCC"<<std::endl;
  return 1;
};
